var balloonsContainer = document.getElementById("balloons-container"),
balloons = balloonsContainer.childNodes,

for (var i = 0; i < balloons.length; i++) {
    var balloon = balloons[i];
    balloon.addEventListener('click', pop, false);
}

function pop(e) {
    var element = e.target;
    element.style.display = "none";
    e.preventDefault();
    e.stopPropagation();
}
